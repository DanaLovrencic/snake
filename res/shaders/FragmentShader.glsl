#version 460 core

in vec3 fragment_color;
layout(location = 0) out vec3 color;

void main()
{
    color = fragment_color;
}
