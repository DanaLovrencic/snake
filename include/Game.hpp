#ifndef GAME_HPP
#define GAME_HPP

#include "Snake.hpp"
#include "Grid.hpp"
#include "Border.hpp"
#include "Timestep.hpp"
#include "Control.hpp"
#include "Configuration.hpp"

#include <memory>
#include <glm/gtc/matrix_transform.hpp>

struct DrawSettings {
    glm::mat4 proj;
    glm::mat4 view        = glm::mat4(1.0f);
    ShaderProgram program = ShaderProgram("res/shaders/VertexShader.glsl",
                                          "res/shaders/FragmentShader.glsl");

    DrawSettings(int rows, int cols)
        : proj(glm::ortho(0.0f, (float)cols, (float)rows, 0.0f))
    {}
};

class Game {
  private:
    Window& window;
    Snake snake;
    Food food;
    Grid grid;
    Border border;

    Configuration config; 
    std::unique_ptr<Control> control;

    const unsigned max_score;
    unsigned score                 = 0;
    float time_accumulator         = 0;
    Timepoint last_frame_timepoint = Timestep::get_current_timepoint();

    DrawSettings& ds;
    Renderer renderer;

    bool pause = false;

  public:
    Game(Window& window, Configuration& config, DrawSettings& ds);
    void start();
    bool is_over() const;
    unsigned get_score() const;

  private:
    void update_game_state(Timestep timestep);
    void set_direction(Direction current_direction);
    void render();
};

#endif
