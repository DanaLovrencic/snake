#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "Window.hpp"
#include "Node.hpp"

#include <algorithm>
#include <deque>
#include <memory>
#include <stack>
#include <optional>

class Control {
  public:
    virtual ~Control() = default;

    virtual Key get_key() = 0;

    enum class Type { Keyboard, BFS, DFS, LongestPath, Hamilton };
};

class KeyboardControl : public Control {
  public:
    KeyboardControl(const Window& w, Key start);

    Key get_key() override;
};

/* Performs BFS by default. */
class SearchAlgorithmControl : public Control {
  protected:
    std::deque<Key> keys;

  protected:
    std::deque<Node> closed; // For path reconstruction.

    const Snake& snake;
    const Food& food;
    const Window& window;

  protected:
    class OpenNodesCollection {
      private:
        std::deque<Node> open;

      public:
        virtual void insert(const Node& node);
        virtual bool empty();
        virtual Node remove_head();
        virtual void clear();
    };

  public:
    SearchAlgorithmControl(const Snake& snake,
                           const Food& food,
                           const Window& window);
    Key get_key() override;
    std::deque<Node> reconstruct_nodes(const Node& node);
    virtual std::optional<Node> search(const Node& start_node,
                                       const Node& goal_node);

  protected:
    virtual std::unique_ptr<OpenNodesCollection>
    get_empty_open_nodes_collection() const;
    std::vector<Node> get_successors(const Node& node) const;
    std::vector<Node> get_all_successors(const Node& node) const;
    std::deque<State> reconstruct_path(const Node& node) const;
    static std::deque<Key>
    from_states_to_keys(const std::deque<State>& rec_path);

  private:
    bool is_goal(const State& state);

    bool is_node_vaild(const Node& node) const;
    bool is_node_border(const Node& node) const;
};

class BFSControl : public SearchAlgorithmControl {
  public:
    BFSControl(const Snake& snake, const Food& food, const Window& window);
};

class DFSControl : public SearchAlgorithmControl {
  protected:
    class DFSOpenNodesCollection : public OpenNodesCollection {
      private:
        std::stack<Node> open;

      public:
        void insert(const Node& node) override;
        bool empty() override;
        Node remove_head() override;
        void clear() override;
    };

  public:
    DFSControl(const Snake& snake, const Food& food, const Window& window);

  protected:
    std::unique_ptr<OpenNodesCollection>
    get_empty_open_nodes_collection() const override;
};

class LongestPathControl : public SearchAlgorithmControl {
  private:
    BFSControl shortest_path_control;

  public:
    LongestPathControl(const Snake& snake,
                       const Food& food,
                       const Window& window);

  protected:
    std::optional<Node> search(const Node& node,
                                    const Node& goal_node) override;

  private:
    static bool contains_state(const Node& check_node,
                               const std::deque<Node>& nodes);
    static bool contains_state(const Node& check_node,
                               const std::vector<Node>& nodes);
};

class HamiltonianPathControl : public LongestPathControl {
  public:
    HamiltonianPathControl(const Snake& snake,
                           const Food& food,
                           const Window& window);

    Key get_key() override;
};

#endif
