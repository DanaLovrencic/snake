#ifndef VERTEX_BUFFER_HPP
#define VERTEX_BUFFER_HPP

class VertexBuffer {
  private:
    unsigned id;

  public:
    VertexBuffer(const void* data, unsigned size);
    ~VertexBuffer();

    void bind() const;
    void unbind() const;
    void set_vertices(const void* data, unsigned size);
};

#endif
