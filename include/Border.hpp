#ifndef BORDER_HPP
#define BORDER_HPP

#include "RenderObject.hpp"

class Border : public RenderObject {
  private:
    unsigned rows, cols;

  public:
    Border(unsigned rows,
         unsigned cols,
         const glm::mat4& proj,
         const glm::mat4& view,
         ShaderProgram& program);

  private:
    void set_vertices() override;
    void set_indices() override;
};

#endif
