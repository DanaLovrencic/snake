#ifndef VERTEX_BUFFER_LAYOUT_HPP
#define VERTEX_BUFFER_LAYOUT_HPP

#include <vector>

#include <GL/glew.h>

struct VertexBufferElement {
    unsigned type;
    unsigned count;
    bool normalized;

    static unsigned int get_size_of_type(unsigned type);
};

class VertexBufferLayout {
  private:
    std::vector<VertexBufferElement> elements;
    unsigned stride;

  public:
    VertexBufferLayout() : stride(0){};

    template<typename T>
    void push(unsigned count); // Use specializations.

    inline const std::vector<VertexBufferElement>& get_elements() const
    {
        return elements;
    }

    inline unsigned get_stride() const { return stride; }
};

#endif
