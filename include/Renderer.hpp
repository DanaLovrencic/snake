#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "VertexArray.hpp"
#include "IndexBuffer.hpp"
#include "Shader.hpp"

enum class RenderPrimitive { POINT, LINE, TRIANGLE, TRIANGLE_FAN };

class Renderer {
  private:
    RenderPrimitive primitive;

  public:
    Renderer(RenderPrimitive primitive = RenderPrimitive::TRIANGLE);
    void draw(const VertexArray& va,
              const IndexBuffer& ib,
              const ShaderProgram& program) const;

    void clear() const;
    void set_dark_background() const;
    void set_primitive(RenderPrimitive primitive);

    Renderer(const Renderer& other) = delete;
    void operator=(const Renderer& other) = delete;

  private:
    unsigned get_gl_primitive() const;
};

#endif
