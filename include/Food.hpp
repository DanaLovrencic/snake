#ifndef FOOD_HPP
#define FOOD_HPP

#include "RenderObject.hpp"
#include "State.hpp"

class Snake;

class Food : public RenderObject {
  private:
    State state;

  public:
    Food(const glm::mat4& proj, const glm::mat4& view, ShaderProgram& program);

    void generate(const Snake& states_to_avoid, unsigned rows, unsigned cols);

    const State& get_state() const;

  protected:
    void set_vertices() override;
    void set_indices() override;

  private:
    bool contains(const std::vector<State> states, const State& state) const;
};

#endif
