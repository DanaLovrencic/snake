#ifndef TIMESTEP_HPP
#define TIMESTEP_HPP

#include <chrono>

using TimestepClock = std::chrono::high_resolution_clock;
using Timepoint = TimestepClock::time_point;
using Duration = TimestepClock::duration;

class Timestep {
  private:
    Duration time;

  public:
    Timestep(Duration time = std::chrono::seconds(0));

    std::chrono::seconds get_seconds() const;
    std::chrono::milliseconds get_milliseconds() const;
    std::chrono::nanoseconds get_nanoseconds() const;

    operator float() const;

    static Timepoint get_current_timepoint();
};

#endif
