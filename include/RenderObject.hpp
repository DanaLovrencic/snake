#ifndef RENDER_OBJECT_HPP
#define RENDER_OBJECT_HPP

#include "VertexArray.hpp"
#include "Shader.hpp"
#include "IndexBuffer.hpp"
#include "Renderer.hpp"

class Vertex {
  public:
    glm::vec2 pos;
    glm::vec3 color;

  public:
    Vertex(glm::vec2 pos, glm::vec3 color);
    Vertex() = default;
};

class RenderObject {
  private:
    RenderPrimitive render_primitive;

  protected:
    VertexArray va;
    ShaderProgram& program;
    const glm::mat4& proj;
    const glm::mat4& view;
    glm::mat4 model = glm::mat4(1.0f);
    std::unique_ptr<IndexBuffer> ib;
    std::unique_ptr<VertexBuffer> vb;

    std::vector<Vertex> vertices;
    std::vector<unsigned> indices;

  public:
    RenderObject(ShaderProgram& program,
                 const glm::mat4& proj,
                 const glm::mat4& view,
                 RenderPrimitive render_primitive);

    virtual void draw(Renderer& renderer);
    virtual void set_vertices() = 0;
    virtual void set_indices() = 0;
};

#endif
