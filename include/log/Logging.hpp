#ifndef LOGGING_HPP
#define LOGGING_HPP

#include <stdexcept>

#include <iostream>

namespace Log {

void report_error(const std::exception& err);
void report_warning(const std::string& war);
void report_note(const std::string& note);
void report_success(const std::string& suc);

}

#endif
