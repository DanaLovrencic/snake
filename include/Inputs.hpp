#ifndef INPUTS_HPP
#define INPUTS_HPP

#include <deque>

enum class Key { Left, Right, Up, Down, None, Pause };

Key openGL_to_key(int key_code);

#endif
