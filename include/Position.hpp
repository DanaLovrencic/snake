#ifndef POSITION_HPP
#define POSITION_HPP

#include <ostream>

class Position {
  public:
    float x, y; // x, y = row, col number
  public:
    Position() = default;
    Position(float x, float y);

    friend bool operator==(const Position& first, const Position& second);
    friend std::ostream& operator<<(std::ostream& os, const Position& position);
};

#endif
