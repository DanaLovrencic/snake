# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
# ImGui static library.
LIBRARY = imgui
LIBRARY_FILE = lib$(LIBRARY).a
CXX = g++
AR = ar

# Source and object directories.
SRC_DIR = imgui
OBJ_DIR = obj/imgui
# ------------------------------------------------------------------------------


# ------------------------------- COMPILER FLAGS -------------------------------
# Configuration flags.
DEBUG_FLAGS = -ggdb3 -O0
RELEASE_FLAGS = -O3 -march=native

# Preprocessor and compiler flags.
CPPFLAGS = -Wall -Wextra -pedantic-errors -MMD -MP
CXXFLAGS = -std=c++17
# ------------------------------------------------------------------------------


# ------------------------------ HELPER COMMANDS -------------------------------
# Directory guard. Used to create directory if a file requires it.
DIRECTORY_GUARD = @mkdir -p $(@D)
# ------------------------------------------------------------------------------


# --------------------------- SOURCE AND OBJECT FILES --------------------------
# All source files.
SRC = $(shell find $(SRC_DIR) -maxdepth 1 -name '*.cpp')

# All object files.
OBJ = $(SRC:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)

# Make dependency files - with paths from the project root.
DEP = $(OBJ:.o=.d)
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: clean purge debug release
# ------------------------------------------------------------------------------


# ------------------------------ BUILDING RECIPES ------------------------------
# Debug build.
debug: CPPFLAGS += $(DEBUG_FLAGS)
debug: $(LIBRARY_FILE)

# Release build.
release: CPPFLAGS += $(RELEASE_FLAGS)
release: $(LIBRARY_FILE)

# Archiving (creating the static library).
$(LIBRARY_FILE): $(OBJ)
	$(AR) rcs $@ $^

# Compiling source files.
$(OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	$(DIRECTORY_GUARD)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove object files.
clean:
	$(RM) $(OBJ) $(DEP)

# Remove object directories and executable files.
purge:
	$(RM) $(OBJ_DIR) -r
	$(RM) $(LIBRARY_FILE)
# ------------------------------------------------------------------------------


# ---------------------------------- OTHER -------------------------------------
# Make source file recompile if header file that it includes has changed. This
# requires -MD flag passed to the compiler.
-include $(DEP)
# ------------------------------------------------------------------------------
