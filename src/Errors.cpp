#include "Errors.hpp"

#include <GL/glew.h> // Has to be included before GLFW.
#include <GLFW/glfw3.h>

#include <stdexcept>
#include <iostream>

namespace Errors {

/* Callback function called when error occurs. */
static void opengl_callback_function(GLenum,
                                     GLenum,
                                     GLuint id,
                                     GLenum severity,
                                     GLsizei,
                                     const GLchar* message,
                                     const void*)
{
    if (id == 131218) return; // Ignore NVIDIA shader performance warning.

    // Throw an exception at any level of severity except notifications.
    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH:
        case GL_DEBUG_SEVERITY_MEDIUM:
        case GL_DEBUG_SEVERITY_LOW: throw std::runtime_error(message);
    }
}

void enable()
{
    // Ensure debug messages are enabled.
    glEnable(GL_DEBUG_OUTPUT);

    // Ensure callback is called by same thread so stack-trace is meaningful.
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    // Register a callback function.
    glDebugMessageCallback(opengl_callback_function, nullptr);

    // Ensure all possible callback messages are recieved.
    glDebugMessageControl(
        GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, true);
}

}
