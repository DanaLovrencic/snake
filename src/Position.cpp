#include "Position.hpp"

Position::Position(float x, float y) : x(x), y(y) {}

bool operator==(const Position& first, const Position& second)
{
    return first.x == second.x && first.y == second.y;
}

std::ostream& operator<<(std::ostream& os, const Position& position)
{
    return os << '(' << position.x << ' ' << position.y << ')';
}
