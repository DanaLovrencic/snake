#include "Shader.hpp"

#include <fstream>
#include <sstream>

#include "log/Logging.hpp"

#include <GL/glew.h>

Shader::Shader(unsigned type, const std::string& shader_filename) : type(type)
{
    std::ifstream ifs(shader_filename, std::ios::in);
    std::stringstream sstr;
    if (!ifs.is_open()) {
        sstr << "Failed to open shader file: " << shader_filename;
        throw std::runtime_error(sstr.str());
    }
    sstr << ifs.rdbuf();
    code = sstr.str();
    ifs.close();
}

Shader::~Shader()
{
    glDeleteShader(shader_id);
}

void Shader::compile()
{
    shader_id      = glCreateShader(type);
    char* code_ptr = const_cast<char*>(code.c_str());
    glShaderSource(shader_id, 1, &code_ptr, nullptr);
    glCompileShader(shader_id);

    // Error handling.
    int result;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) { // Shader did not compile properly.
        int message_length;   // Error message length.
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &message_length);
        std::string message(message_length + 1, '\0'); // Reserve message space.
        glGetShaderInfoLog(shader_id,
                           message_length,
                           &message_length,
                           const_cast<char*>(message.c_str()));
        glDeleteShader(shader_id);
        std::stringstream sstr;
        sstr << "Failed to compile "
             << (type == GL_VERTEX_SHADER ? "vertex" : "fragment")
             << " shader!\n"
             << message << std::endl;
        throw std::runtime_error(sstr.str());
    }
}

void Shader::attach(unsigned program_id) const
{
    glAttachShader(program_id, shader_id);
}

void Shader::detach(unsigned program_id) const
{
    glDetachShader(program_id, shader_id);
}

ShaderProgram::ShaderProgram(const std::string& vertex_shader_filename,
                             const std::string& fragment_shader_filename)
    : program_id(glCreateProgram())
{
    // Load, compile and attach vertex shader.
    Shader vertex(GL_VERTEX_SHADER, vertex_shader_filename);
    vertex.compile();
    vertex.attach(program_id); // Attach vertex shader to shader program.

    // Load, compile and attach fragment shader.
    Shader fragment(GL_FRAGMENT_SHADER, fragment_shader_filename);
    fragment.compile();
    fragment.attach(program_id); // Attach fragment shader to shader program.

    // Link and validate.
    glLinkProgram(program_id);
    glValidateProgram(program_id);
    int validation_result;
    glGetProgramiv(program_id, GL_VALIDATE_STATUS, &validation_result);
    if (validation_result == GL_FALSE) {
        throw std::runtime_error("Failed to validate linked shader program.");
    }

    // Detach used shaders.
    fragment.detach(program_id);
    vertex.detach(program_id);
}

ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(program_id);
}

void ShaderProgram::bind() const
{
    glUseProgram(program_id);
}

void ShaderProgram::unbind() const
{
    glUseProgram(0);
}

void ShaderProgram::set_uniform4f(const std::string& name,
                                  const glm::vec4& value)
{
    glUniform4f(
        get_uninform_location(name), value.x, value.y, value.z, value.w);
}

void ShaderProgram::set_uniform3f(const std::string& name,
                                  const glm::vec3& value)
{
    glUniform3f(get_uninform_location(name), value.x, value.y, value.z);
}

void ShaderProgram::set_uniform1i(const std::string& name, int value)
{
    glUniform1i(get_uninform_location(name), value);
}

void ShaderProgram::set_uniform_mat4f(const std::string& name,
                                      const glm::mat4& value)
{
    glUniformMatrix4fv(get_uninform_location(name), 1, GL_FALSE, &value[0][0]);
}

int ShaderProgram::get_uninform_location(const std::string& name)
{
    if (uniform_location_cache.find(name) != uniform_location_cache.end()) {
        return uniform_location_cache[name];
    }

    int location = glGetUniformLocation(program_id, name.c_str());
    if (location == -1) {
        std::stringstream ss;
        ss << "Uniform '" << name
           << "' does not exist or is not used in "
              "the shader.";
        Log::report_warning(ss.str());
    }

    uniform_location_cache[name] = location;
    return location;
}
