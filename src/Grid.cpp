#include "Grid.hpp"
#include <cmath>

Grid::Grid(unsigned rows,
           unsigned cols,
           const glm::mat4& proj,
           const glm::mat4& view,
           ShaderProgram& program)
    : RenderObject(program, proj, view, RenderPrimitive::TRIANGLE),
      rows(rows),
      cols(cols)
{
    vertices.resize(rows * cols * 2 + 1); // *2?
    indices.resize(rows * cols * 6);      // *3 ?

    set_vertices();
    set_indices();

    vb = std::make_unique<VertexBuffer>(&vertices[0],
                                        vertices.size() * sizeof(Vertex));
    ib = std::make_unique<IndexBuffer>(&indices[0], indices.size());

    VertexBufferLayout layout;
    layout.push<float>(2); // Vertex buffer has 2 integers.
    layout.push<float>(3); // Vertex buffer has 3 integers.
    va.add_buffer(*vb, layout);
}

void Grid::set_vertices()
{
    glm::vec3 color = glm::vec3(1.0f, 1.0f, 1.0f);

    size_t k = 0;
    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < cols; j++) {
            if ((i % 2 && !(j % 2)) || (!(i % 2) && j % 2)) {
                vertices[0 + 4 * k] = Vertex(glm::vec2(j, i), color);
                vertices[1 + 4 * k] = Vertex(glm::vec2(j + 1, i), color);
                vertices[2 + 4 * k] = Vertex(glm::vec2(j, i + 1), color);
                vertices[3 + 4 * k] = Vertex(glm::vec2(j + 1, i + 1), color);
                k++;
            }
        }
    }
}

void Grid::set_indices()
{
    for (size_t i = 0; i < rows * cols; i++) {
        indices[0 + 6 * i] = 0 + 4 * i;
        indices[1 + 6 * i] = 1 + 4 * i;
        indices[2 + 6 * i] = 2 + 4 * i;
        indices[3 + 6 * i] = 2 + 4 * i;
        indices[4 + 6 * i] = 3 + 4 * i;
        indices[5 + 6 * i] = 1 + 4 * i;
    }
}
