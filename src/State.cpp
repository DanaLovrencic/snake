#include "State.hpp"
#include <algorithm>

State::State(Position position) : position(position) {}

void State::set_position(Position position)
{
    this->position = position;
}

bool operator==(const State& first, const State& second)
{
    return first.position == second.position;
}

namespace std {
size_t hash<State>::operator()(const State& state) const
{
    return hash<float>()(state.position.x) ^ hash<float>()(state.position.y);
}
}

bool State::is_border(int rows, int cols) const
{
    return (position.x == (int)(rows - 1) || position.x <= 0 ||
            position.y == (int)(cols - 1) || position.y <= 0);
}

std::ostream& operator<<(std::ostream& os, const State& state)
{
    return os << state.position;
}
