#include "Game.hpp"
#include "Configuration.hpp"
#include "Renderer.hpp"

#include "imgui.h"
#include "examples/imgui_impl_glfw.h"
#include "examples/imgui_impl_opengl3.h"

#include <iostream>
bool LoadTextureFromFile(const char* filename,
                         GLuint* out_texture,
                         int* out_width,
                         int* out_height);

int main()
{
    Window window(12, 12, 700, 700, "Zmijica", true);
    Renderer r;
    Configuration config;
    DrawSettings ds(window.rows, window.cols);

    bool bfs_pressed  = false;
    bool dfs_pressed  = false;
    bool kybr_pressed = false;
    bool lp_pressed   = false;
    bool h_pressed    = false;

    while (window.open()) {
        while (window.open() && !(kybr_pressed || bfs_pressed || dfs_pressed ||
                                  lp_pressed || h_pressed)) {
            r.set_dark_background();

            // IMGUI ---------------------------------------- BEGIN
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
            // IMGUI =========================================

            ImGui::Begin("Button",
                         nullptr,
                         ImGuiWindowFlags_NoMove |
                             ImGuiWindowFlags_NoDecoration |
                             ImGuiWindowFlags_AlwaysAutoResize |
                             ImGuiWindowFlags_NoBackground);
            ImGui::Text("Solvers:");
            ImGui::Spacing();

            ImGui::PushID(0);
            ImGui::PushStyleColor(
                ImGuiCol_Button,
                static_cast<ImVec4>(ImColor::HSV(0.1f, 0.0f, 0.2f)));
            ImGui::PushStyleColor(
                ImGuiCol_ButtonHovered,
                static_cast<ImVec4>(ImColor::HSV(0.5f, 0.0f, 0.3f)));
            ImGui::PushStyleColor(
                ImGuiCol_ButtonActive,
                static_cast<ImVec4>(ImColor::HSV(0.6f, 0.0f, 0.4f)));

            constexpr int indent = 40;
            ImGui::SetCursorPosX(indent);
            kybr_pressed = ImGui::Button("Keyboard");
            ImGui::SetCursorPosX(indent);
            bfs_pressed = ImGui::Button("Breadth first search ");
            ImGui::SetCursorPosX(indent);
            dfs_pressed = ImGui::Button("Depth First Search");
            ImGui::SetCursorPosX(indent);
            lp_pressed = ImGui::Button("Longest Path");
            ImGui::SetCursorPosX(indent);
            h_pressed = ImGui::Button("Hamiltonian Cycle");

            ImGui::PopStyleColor(3);
            ImGui::PopID();

            if (kybr_pressed) { config.control_type = Control::Type::Keyboard; }
            if (bfs_pressed) { config.control_type = Control::Type::BFS; }
            if (dfs_pressed) { config.control_type = Control::Type::DFS; }
            if (lp_pressed) {
                config.control_type = Control::Type::LongestPath;
            }
            if (h_pressed) { config.control_type = Control::Type::Hamilton; }

            int my_image_width      = 0;
            int my_image_height     = 0;
            GLuint my_image_texture = 0;
            bool ret                = LoadTextureFromFile("Snake.png",
                                           &my_image_texture,
                                           &my_image_width,
                                           &my_image_height);
            if (!ret) throw std::runtime_error("Failed to load snake image.");
            ImGui::SetCursorPos({240, 270});
            ImGui::Image((void*)(intptr_t)my_image_texture, ImVec2(400, 400));

            ImGui::End();

            // IMGUI ---------------------------------------- END
            // Set display size.
            ImGuiIO& io = ImGui::GetIO();
            io.DisplaySize =
                ImVec2(window.dimensions().first, window.dimensions().second);

            // Render ImGui to the screen.
            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
            // IMGUI =========================================

            window.poll_inputs();
            window.swap_buffers();
        }

        Game game(window, config, ds);
        game.start();

        kybr_pressed = false;
        bfs_pressed  = false;
        dfs_pressed  = false;
        lp_pressed   = false;
        h_pressed    = false;
    }
}

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

// Simple helper function to load an image into a OpenGL texture with common
// settings
bool LoadTextureFromFile(const char* filename,
                         GLuint* out_texture,
                         int* out_width,
                         int* out_height)
{
    // Load from file
    int image_width  = 0;
    int image_height = 0;
    unsigned char* image_data =
        stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL) return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 image_width,
                 image_height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width   = image_width;
    *out_height  = image_height;

    return true;
}
