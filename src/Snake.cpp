#include "Snake.hpp"

#include <algorithm>

Snake::Snake(Food& food,
             const glm::mat4& proj,
             const glm::mat4& view,
             ShaderProgram& program)
    : RenderObject(program, proj, view, RenderPrimitive::TRIANGLE),
      food(food),
      states(std::vector<State>({State(Position(6, 4)), State(Position(6, 5))}))
{
    vertices.resize(5000); // FIXME
    indices.resize(5000);

    set_vertices();
    set_indices();

    ib = std::make_unique<IndexBuffer>(&indices[0], indices.size());
    vb = std::make_unique<VertexBuffer>(&vertices[0],
                                        vertices.size() * sizeof(Vertex));

    VertexBufferLayout layout;
    layout.push<float>(2); // Position.
    layout.push<float>(3); // Color.
    va.add_buffer(*vb, layout);
}

void Snake::change_direction(Direction direction)
{
    this->direction = direction;
}

Direction Snake::get_direction() const
{
    return direction;
}

void Snake::update_buffers()
{
    set_vertices();
    set_indices();

    // Update buffers for rendering.
    ib->set_indices(&indices[0], indices.size());
    vb->set_vertices(&vertices[0], vertices.size() * sizeof(Vertex));
}

void Snake::pop_tail()
{
    states.pop_back();
}

bool Snake::is_head_on(const Food& food) const
{
    return get_head() == food.get_state();
}

bool Snake::is_head_on_body() const
{
    const State& head = get_head();
    return std::find(states.begin() + 1, states.end(), head) != states.end();
}

bool Snake::is_head_on_border(unsigned rows, unsigned cols) const
{
    const State& head = get_head();
    return (head.position.x == (int)(rows - 1) || head.position.x <= 0 ||
            head.position.y == (int)(cols - 1) || head.position.y <= 0);
}

bool Snake::contains(const State& state) const
{
    return std::find(states.begin(), states.end(), state) != states.end();
}

const State& Snake::get_head() const
{
    return states[0];
}

void Snake::set_vertices()
{
    glm::vec3 color(0.15f, 0.15f, 0.15f);
    constexpr glm::vec3 head_color(0.05f, 0.05f, 0.05f);
    constexpr glm::vec3 tail_color(0.6f, 0.6f, 0.6f);
    // constexpr glm::vec3 body_color = glm::vec3(0.45f, 0.45f, 0.45f);
    const Position& p = states[0].position;

    set_cell_vertices(p.x, p.y, head_color, 0);
    for (size_t i = 1; i < states.size(); i++) {
        const Position& p = states[i].position;
        set_cell_vertices(p.x, p.y, color, i);
        color += 0.006f;
        if (color.r > 0.9) { color = glm::vec3(0.9f); }
        if (i == states.size() - 1) {
            set_cell_vertices(p.x, p.y, tail_color, i);
        }
    }
}

const std::vector<State>& Snake::get_body_parts() const
{
    return states;
}

void Snake::set_indices()
{
    for (std::size_t i = 0; i < states.size(); i++) {
        // First triangle.
        indices[0 + 6 * i] = 0 + 4 * i;
        indices[1 + 6 * i] = 1 + 4 * i;
        indices[2 + 6 * i] = 2 + 4 * i;

        // Second triangle.
        indices[3 + 6 * i] = 2 + 4 * i;
        indices[4 + 6 * i] = 3 + 4 * i;
        indices[5 + 6 * i] = 1 + 4 * i;
    }
}

// Sets coordinates and color for vertices of one cell on board.
void Snake::set_cell_vertices(float x,
                              float y,
                              const glm::vec3 color,
                              size_t index)
{
    vertices[0 + 4 * index] = Vertex(glm::vec2(y, x), color);
    vertices[1 + 4 * index] = Vertex(glm::vec2(y + 1, x), color);
    vertices[2 + 4 * index] = Vertex(glm::vec2(y, x + 1), color);
    vertices[3 + 4 * index] = Vertex(glm::vec2(y + 1, x + 1), color);
}

// Creates new head depending on current snake direction.
void Snake::update_head()
{
    int delta_x, delta_y;
    switch (direction) {
        case (Direction::Left):
            delta_x = 0;
            delta_y = -1;
            break;
        case (Direction::Right):
            delta_x = 0;
            delta_y = 1;
            break;
        case (Direction::Up):
            delta_x = -1;
            delta_y = 0;
            break;
        case (Direction::Down):
            delta_x = 1;
            delta_y = 0;
            break;
        default: throw std::runtime_error("[SNAKE] Unknown direction.");
    }

    const State& head = get_head();
    Position new_pos =
        Position(head.position.x + delta_x, head.position.y + delta_y);

    states.insert(states.begin(), State(new_pos));
}

const State& Snake::get_tail() const
{
    return states[states.size() - 1];
}
