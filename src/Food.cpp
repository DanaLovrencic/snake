#include "Food.hpp"
#include "Random.hpp"

#include "Snake.hpp"

#include <random>
#include <algorithm>

Food::Food(const glm::mat4& proj, const glm::mat4& view, ShaderProgram& program)
    : RenderObject(program, proj, view, RenderPrimitive::TRIANGLE_FAN),
      state(State(Position(1, 3)))
{
    constexpr float n_segments = 200;
    vertices.resize(n_segments + 1);
    indices.resize(n_segments + 2);

    set_vertices();
    set_indices();

    vb = std::make_unique<VertexBuffer>(&vertices[0],
                                        vertices.size() * sizeof(Vertex));
    ib = std::make_unique<IndexBuffer>(&indices[0], indices.size());

    VertexBufferLayout layout;
    layout.push<float>(2); // Position.
    layout.push<float>(3); // Color.

    va.add_buffer(*vb, layout);
}

void Food::generate(const Snake& units_to_avoid, unsigned rows, unsigned cols)
{
    const std::vector<State> occupied_states = units_to_avoid.get_body_parts();
    // TODO implement with std::set and std::set_difference()
    std::vector<State> all_states;
    std::vector<State> free_states;

    for (unsigned i = 1; i < rows - 1; i++) {
        for (unsigned j = 1; j < cols - 1; j++) {
            all_states.push_back({Position(i, j)});
        }
    }

    for (auto& a : all_states) {
        if (!contains(occupied_states, a)) { free_states.push_back(a); }
    }

    if (free_states.size() == 0) {
        return;
    }
    std::uniform_int_distribution<int> dist(0, free_states.size() - 1);

    unsigned index = dist(Random::generator());
    auto [x, y]    = free_states.at(index).position;
    state.position = Position(x, y);

    set_vertices();
    set_indices();
    vb->set_vertices(&vertices[0], vertices.size() * sizeof(Vertex));
    ib->set_indices(&indices[0], indices.size());
}

const State& Food::get_state() const
{
    return state;
}

void Food::set_vertices()
{
    constexpr float n_segments = 200;
    float x                    = state.position.x;
    float y                    = state.position.y;
    glm::vec2 center = glm::vec2((y + (y + 1)) / 2, (x + (x + 1)) / 2);
    float radius     = 0.45;

    glm::vec3 color = glm::vec3(0.1f, 0.1f, 0.1f);

    vertices[0] = Vertex(center, color);
    // Draw circle.
    for (size_t i = 0; i < n_segments; i++) {
        float alpha = 2.0f * 3.1415926f * static_cast<float>(i) / n_segments;
        float x     = center.x + radius * std::cos(alpha);
        float y     = center.y + radius * std::sin(alpha);
        vertices[i + 1] = Vertex(glm::vec2(x, y), color);
    }
}

void Food::set_indices()
{
    for (size_t i = 0; i < indices.size(); i++) indices[i] = i;
    indices.back() = indices[1];
}

bool Food::contains(const std::vector<State> states, const State& state) const
{
    return std::find(states.begin(), states.end(), state) != states.end();
}
