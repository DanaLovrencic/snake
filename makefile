# PROJECT STRUCTURE:
#      ProjectRoot
#       |- src       # Source files. Can contain subdirectories.
#       |- include   # Header files. Can contain subdirectories.
#       |- obj       # Intermediate (object) files. Mirrors source tree.
#       |- res       # Project resources.
#       |- exe       # Executable. Built with 'make'.
#       |- test      # Test source files.
#           |- obj   # Object files of test sources. Mirrors test source tree.
#           |- exe   # Test executable. Built with 'make test'.


# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
EXECUTABLE = Snake
CXX = g++

# Directory structure.
INCLUDE_DIR = include
SRC_DIR = src
OBJ_DIR = obj
LIB_DIR = lib
TEST_DIR = test

# ImGui library settings.
IMGUI_LIB_NAME = imgui
IMGUI_INCLUDE  = $(LIB_DIR)/imgui
IMGUI_LIB      = $(LIB_DIR)/lib$(IMGUI_LIB_NAME).a
IMGUI_MAKEFILE = imgui.mk

# File containing main function definition. Excluded from test compilation.
MAIN_SOURCE_FILE = src/Main.cpp
# ------------------------------------------------------------------------------


# ------------------------------- COMPILER FLAGS -------------------------------
# Configuration flags.
DEBUG_FLAGS = -ggdb3 -O0
RELEASE_FLAGS = -O3 -march=native -DNDEBUG

# Preprocessor and compiler flags.
CPPFLAGS  = -Wall -Wextra -pedantic-errors -MMD -MP -I$(INCLUDE_DIR)
CPPFLAGS += -I$(IMGUI_INCLUDE)
CXXFLAGS  = -std=c++17
# ------------------------------------------------------------------------------


# -------------------------------- LINKER FLAGS --------------------------------
# Linker flags and libraries to link against.
LDFLAGS = -L$(LIB_DIR)
LDLIBS = -lGL -lGLEW -lglfw -l$(IMGUI_LIB_NAME)
# ------------------------------------------------------------------------------


# ------------------------------ HELPER COMMANDS -------------------------------
# Directory guard. Used to create directory if a file requires it.
DIRECTORY_GUARD = @mkdir -p $(@D)
# ------------------------------------------------------------------------------


# --------------------------- SOURCE AND OBJECT FILES --------------------------
# All source files.
SRC = $(shell find $(SRC_DIR) -name '*.cpp')

# All object files.
OBJ = $(SRC:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)

# Make dependency files - with paths from the project root.
DEP = $(OBJ:.o=.d)
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: clean purge debug release test
# ------------------------------------------------------------------------------


# ------------------------------ BUILDING RECIPES ------------------------------
# Debug build.
debug: CPPFLAGS += $(DEBUG_FLAGS)
debug: CONFIGURATION = debug
debug: $(EXECUTABLE)

# Release build.
release: CPPFLAGS += $(RELEASE_FLAGS)
release: CONFIGURATION = release
release: $(EXECUTABLE)

# Linking.
$(EXECUTABLE): $(OBJ) $(IMGUI_LIB)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

# Compiling source files.
$(OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	$(DIRECTORY_GUARD)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# ---------------------------------- TESTING -----------------------------------
# Test sources (only test files).
TEST_SRC = $(shell find $(TEST_DIR) -name '*.cpp')

# Test object files (only test files).
TEST_OBJ = $(TEST_SRC:$(TEST_DIR)/%=$(TEST_DIR)/$(OBJ_DIR)/%.o)

# Test make dependency files (only test files).
TEST_DEP = $(TEST_OBJ:.o=.d)

# Object file containing 'main()' function.
MAIN_OBJ = $(MAIN_SOURCE_FILE:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)

# All object files without file containing the 'main()' function.
OBJ_WITHOUT_MAIN = $(filter-out $(MAIN_OBJ), $(OBJ))

# Build the test executable (use debug flags for test compilation).
test: CPPFLAGS += $(DEBUG_FLAGS)
test: $(TEST_DIR)/$(EXECUTABLE)

# Linking the test executable. Filter out object file that contains 'main()'
# definition containing main since testing framework provides entry point.
$(TEST_DIR)/$(EXECUTABLE): $(OBJ_WITHOUT_MAIN) $(TEST_OBJ) $(IMGUI_LIB)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

# Compile test source files.
$(TEST_DIR)/$(OBJ_DIR)/%.cpp.o: $(TEST_DIR)/%.cpp
	$(DIRECTORY_GUARD)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove object files.
clean:
	$(RM) $(OBJ) $(DEP) $(TEST_OBJ) $(TEST_DEP)
	$(MAKE) -C $(LIB_DIR) -f $(IMGUI_MAKEFILE) clean

# Remove object directories and executable files.
purge:
	$(RM) $(OBJ_DIR) $(TEST_DIR)/$(OBJ_DIR) -r
	$(RM) $(TEST_DIR)/$(EXECUTABLE) $(EXECUTABLE)
	$(MAKE) -C $(LIB_DIR) -f $(IMGUI_MAKEFILE) purge
# ------------------------------------------------------------------------------


# ----------------------------------- IMGUI ------------------------------------
$(IMGUI_LIB):
	$(MAKE) -C $(LIB_DIR) -f $(IMGUI_MAKEFILE) $(CONFIGURATION)
# ------------------------------------------------------------------------------


# ------------------------------ COMPILE FLAGS ---------------------------------
compile-flags:
	@printf '%s\n' $(CPPFLAGS) $(CXXFLAGS) > compile_flags.txt
# ------------------------------------------------------------------------------


# ---------------------------------- OTHER -------------------------------------
# Make source file recompile if header file that it includes has changed. This
# requires -MD flag passed to the compiler.
-include $(DEP)
-include $(TEST_DEP)
# ------------------------------------------------------------------------------
